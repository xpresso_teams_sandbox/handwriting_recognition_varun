"""
This is the implementation of data preparation for sklearn
"""

import sys
import logging
from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger
import tensorflow as tf
import numpy as np 
import os

import matplotlib
matplotlib.use('PS')



__author__ = "Varun Kumar"

logger = XprLogger("data_prep",level=logging.INFO)


class DataPrep(AbstractPipelineComponent):
    """ Class used to prepare MNIST data for dnn training

    """

    def __init__(self):
        super().__init__(name="DataPrep")
        (self.x_train_orig, self.y_train_orig), (self.x_test_orig, self.y_test_orig) = tf.keras.datasets.mnist.load_data()

        #Processed X and y sets
        self.x_train = None
        self.x_test = None
        self.y_train = None
        self.y_test = None


    

    def normalize_data(self, X, y):
        x_norm = (X.reshape(X.shape[0], -1))/255.
        y_onehot = tf.one_hot(y, 10, axis = 0)
        y_t = tf.transpose(y_onehot)
        return x_norm, y_t

    def start(self, run_name):
        """
        Performs all operations


        This is the start method, which does the actual data preparation.
        As you can see, it does the following:
          - Calls the superclass start method - this notifies the Controller that
              the component has started processing (details such as the start
              time, etc. are appropriately stored by the Controller)
          - Main data processing or training codebase.
          - It calls the completed method when it is done

        Args:
            run_name: xpresso run name which is used by base class to identify
               the current run. It must be passed. While running as pipeline,
               Xpresso automatically adds it.

        """
        super().start(xpresso_run_name=run_name)
        # === Your start code base goes here ===
        self.x_train, self.y_train = self.normalize_data(X = self.x_train_orig, y = self.y_train_orig)
        self.x_test, self.y_test = self.normalize_data(X = self.x_test_orig, y = y_test_orig)
        logging.info("Data normalized")
        self.send_metrics("Complete x_training set", self.x_train)
        printf("done")
        #self.plot_example(self.x_train, self.y_train, 0)
        self.completed()

        

    def send_metrics(self):
        """ It is called to report intermediate status. It reports status and
        metrics back to the xpresso.ai controller through the report_status
        method of the superclass. The Controller stores any metrics reported in
        a database, and makes these available for comparison. It needs the
        following format:
        - status:
           - status - <single word description>
        - metric:
           - Key-Value - Of the metrics that needs to be tracked and visualized
                         realtime. This could be data size, accuracy, loss etc.
        """
        try:
            report_status = {
                "status": {"status": "data_preparation"},
                "metric": {"metric_key": 1}
            }
            self.report_status(status=report_status)
        except Exception:
            import traceback
            traceback.print_exc()

    def completed(self, push_exp=False, success=True):
        """
        This is the completed method. It stores the output data files on the
        file system, and then calls the superclass completed method, which notes
        the fact that the component has completed processing, along with the end time.

        User must need to call super completed method at the end of the method
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
            success: Use to handle failure cases

        """
        # === Your start code base goes here ===
        output_dir = '/data/dataset/'
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        np.save('/data/dataset/x_train.npy', self.x_train)
        np.save('/data/dataset/y_train.npy', self.y_train)
        np.save('/data/dataset/x_test.npy', self.x_test)
        np.save('/data/dataset/y_test.npy', self.y_test)

        logging.info("Data saved")
        super().completed(push_exp=False)


    


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/main.py

    data_prep = DataPrep() #data_prep object for training
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1])
    else:
        data_prep.start(run_name="")
